package Assignment1;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
import java.net.*;
import java.io.*;
/**
 *
 * @author Ross Bille 3127333
 */
public class AdditionServer 
{
    //machine states
    private static final int STATE0 = 0;//waiting for valid command
    private static final int ADDITION0 = 1;//waiting for first int  
    private static final int ADDITIONPLUS = 2;//waiting for a +
    private static final int ADDITION1 = 3;//waiting for second int
    private static final int ADDITIONEQUALS = 4;//wait for an =
    private static final int STORE = 5;//waiting for a int to store
    private int state = STATE0;
    public static void main (String[] args) throws IOException
    {       
        System.setProperty("line.seperator", "\n");
        int state = 0;
        int sum =0;
        int x = 0;
        int y = 0;
        int memory = 0;
        //System.setProperty("line.seperator", "\n");
        int port = 37334;//default port
        //check if new port has been specified
        if(args.length>0)
        {
            try
            {
                int z = Integer.parseInt(args[0]);
                port = z;
            }
            catch(NumberFormatException e)
            {}
        }
        //set up server
        ServerSocket serverSocket = null;
        try 
        {
            serverSocket = new ServerSocket(port);
        }
        catch (IOException e)
        {
            System.err.println("Could not listen on port: "+port);
            System.exit(1);
        }
        Socket clientSocket = null;
        try
        {
            clientSocket = serverSocket.accept();
        }
        catch(IOException e)
        {
            System.err.println("Accept failed.");
            System.exit(1);
        }
        //set up writers
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
       // Protocol protocol = new Protocol();
       // String outputLine = protocol.processInput(null);
       // out.println(outputLine);
        //start reading
        String inputLine;
        while((inputLine = in.readLine())!=null)
        {
            System.out.println(inputLine);
            System.out.println("NEW LINE");
            try
            {
                int z = Integer.parseInt(inputLine);
                System.out.println("INT" + z);
                if(state == ADDITION0)
                {
                    System.out.println("storing x as"+z);
                    x = z;
                    state = ADDITIONPLUS;
                }else if (state == ADDITION1)
                {
                    System.out.println("storing y as"+z);
                    y = z;
                    state = ADDITIONEQUALS;
                    sum = x+y;
                    out.println(sum);
                    state = ADDITION0;
                }else if(state == STORE)
                {
                    System.out.println("storing "+z+" in memory");
                    memory = z;
                    out.println("STORE: OK");
                    state = ADDITION0;
                }
                out.flush();
            }
            catch(NumberFormatException e)
            {
                System.out.println("STRING"+inputLine);
                if(inputLine.equals("ADD"))
                {
                    out.println("ADD: OK");
                    state = ADDITION0;
                    out.flush();
                }
                if (state == ADDITION0)
                {
                    if(inputLine.equals("MR"))
                    {
                        x=memory;
                        state=ADDITIONPLUS;
                    }else if(inputLine.equals("+")) 
                    {
                        state = ADDITION1;
                    }else if(inputLine.equals("END"))
                    {
                        out.println("END: OK");
                        out.close();
                        in.close();
                        clientSocket.close();
                        serverSocket.close();
                        System.exit(1);
                        //shutdown server
                    }else if(inputLine.equals("BYE"))
                    {
                        out.println("BYE: OK");
                        //clientSocket.close();
                        //shutdown connection
                    }else if(inputLine.equals("MC"))
                    {
                        memory = 0;
                        out.println("CLEAR: OK");
                        //out.flush();
                    }else if(inputLine.equals("MS"))
                    {
                        state = STORE;
                    }
                }else if (state == ADDITION1)
                {
                    if(inputLine.equals("MR"))
                    {
                         System.out.println("storing y as"+inputLine);
                        y=memory;
                        state = ADDITIONEQUALS;
                        sum = x+y;
                        out.println(sum);
                        state = ADDITION0;  
                    }else if(inputLine.equals("=")) 
                    {
                        state = ADDITION0;
                        sum =x+y;
                        out.println(sum);
                    }
                }else if(state == STORE)
                {
                    if(inputLine.equals("#"))
                    {
                        memory = sum;
                        out.println("STORE: OK");
                        state = ADDITION0;
                        out.flush();
                    }
                    
                }else if(state==ADDITIONPLUS)
                {
                    state = ADDITION1;
                }else if(state == ADDITIONEQUALS){
                    sum = x+y;
                    out.println(sum);
                    state = ADDITION0; 
                    
                }
                out.flush();
            }
        }
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
}
