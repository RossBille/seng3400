/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Assignment1;

/**
 *
 * @author Ross Bille 3127333
 */
public class Protocol 
{
    private double memory;
    private double sum,x,y;
    //machine states
    private static final int STATE0 = 0;//waiting for valid command
    private static final int ADDITION0 = 1;//waiting for first double
    private static final int ADDITIONPLUS = 2;//waiting for a +
    private static final int ADDITION1 = 3;//waiting for second double
    private static final int ADDITIONEQUALS = 4;//wait for an =
    private static final int STORE = 5;//waiting for a double to store
    private int state = STATE0;
    //accepted commands
    private static final String[] commands = new String[]{
                    "ADD - add two numbers",
                    "MR - recall memory",
                    "MS - set the memory to a specific value",
                    "MC - clear memory",
                    "HELP - display this help",
                    "BYE - close this session",
                    "END - shutdown server"};
    //constructors
    public Protocol() 
    {
        memory = 0;
        sum = x = y = 0;
    }
    //getters
    public double getMemory() 
    {
        return memory;
    }
    //setters
    public void setMemory(double memory) 
    {
        this.memory = memory;
    }
    //process
    public String processInput(String inputLine) 
    {
        String str = "";
        double z;
        if(!(inputLine == null))
        {
            //check if int or string
            try
            {
                z = Double.parseDouble(inputLine);
                System.out.println("a number has been detected");
                if(state == ADDITION0)
                {
                    x = z;
                    str = "Please input +";
                    state = ADDITIONPLUS;
                }else if(state == ADDITION1)
                {
                    y = z;
                    str = "Please input =";                    
                    state = ADDITIONEQUALS;
                }else if(state == STORE)
                {
                    memory = z;
                    str = "Memory has been set to: "+z;
                }else{
                    //number wasnt expected
                    str = "That is not a valid input, type HELP for assistance resetting state";
                    state = STATE0;
                }
            }
            catch(NumberFormatException e)
            {
                System.out.println("a string has been detected " +inputLine);
                if(inputLine.equalsIgnoreCase("ADD"))
                {
                    str = "Please input an integer.";
                    state = ADDITION0;
                }else if(inputLine.equalsIgnoreCase("+")&& state==ADDITIONPLUS)
                {
                    str = "Please input an integer.";
                    state = ADDITION1;
                }else if(inputLine.equalsIgnoreCase("=")&& state==ADDITIONEQUALS)
                {
                    sum = x+y;
                    str = x + " + " + y + " = " + sum; 
                    state = STATE0;
                }else if(inputLine.equalsIgnoreCase("MC"))
                {
                    memory = 0;
                    str = "Memory cleared, state preserved";
                }else if(inputLine.equalsIgnoreCase("MS")&&state==STATE0)
                {
                    str = "Please supply a new memory value";
                    state = STORE;
                }else if(inputLine.equalsIgnoreCase("HELP")) 
                {
                    //preserve state
                    str = "";
                    for(String s: commands)
                        str += s+","; 
                }else if(inputLine.equalsIgnoreCase("BYE")) 
                {
                    str = "See you soon";
                }else if(inputLine.equalsIgnoreCase("#")&&(state ==STORE))
                {
                    memory = sum;
                }else if(inputLine.equalsIgnoreCase("END"))
                {
                    str = "Shutting down, see you soon";
                }else if(inputLine.equalsIgnoreCase("MR")&&(state == ADDITION0))
                {
                    x=memory;
                }else if(inputLine.equalsIgnoreCase("MR")&&(state == ADDITION1))
                {
                    x=memory;
                }else if(inputLine.equalsIgnoreCase("MR")&&(state!=ADDITION0||state!=ADDITION1))
                {
                     str = memory+" resetting memory";
                     state = STATE0;
                     
                }else{
                    str = "That is not a valid input, type HELP for assistance, resetting state";
                    state = STATE0;
                }
            }
        }
        return str;
    }

}