package Assignment1;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.*;
import java.net.*;
/**
 *
 * @author Ross Bille 3127333
 */
public class AdditionClient 
{
    public static void main(String[] args) throws IOException
    {
        int state = 0;
        int sum =0;
        String n1 = "0";
        String n2 = "0";
        //System.setProperty("line.seperator", "\n");
        Socket additionSocket = null;
        PrintWriter out = null;
        BufferedReader in = null;
        String host = "127.0.0.1";//defualt host
        String defaultMessage = "Enter MS, MC, BYE, END, or a value to add (non-negative integer or MR for memory): ";
        String negativeMessage = "Enter a value to add (non-negative integer or MR for memory): "; 
        int port = 37334;//default port
        //check if different port and host have been specified
        for(String str:args)
        {
            try
            {
                int x = Integer.parseInt(str);
                port = x;
            }
            catch(NumberFormatException e)
            {
                host = str;
            }
        }
        try
        {
            additionSocket = new Socket(host,port);
            out = new PrintWriter(additionSocket.getOutputStream(),true);
            in = new BufferedReader(new InputStreamReader(additionSocket.getInputStream()));
        }
        catch(UnknownHostException e)
        {
            System.err.println("Can't find host: "+host);
            System.exit(1);
        }
        catch(IOException e)
        {
            System.err.println("Couldnt get I/O for "+host);
            System.exit(1);
        }
        System.out.println("Connecting to host : "+host);
        System.out.println("Connecting through port:"+port);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
        String input;
        //
       // System.out.println(in.readLine());
        //out.flush();
        //send ADD to server to create session
        System.out.println("CLIENT: ADD");
        out.println("ADD");
        out.flush();
        String serversMessage = in.readLine();
        System.out.println(serversMessage);
        System.out.println(defaultMessage);
        while ((input=stdIn.readLine()) != null) 
        { 
            //shut down client
            if(input.equals("BYE"))
            {
                System.out.println("CLIENT: BYE");
                out.println("BYE");
                out.flush();
                System.out.println("SERVER: "+in.readLine());
                out.close();
                in.close();
                stdIn.close();
                additionSocket.close();
                System.exit(1);
            }
            
            //shut down client and server
            if(input.equals("END"))
            {
                System.out.println("CLIENT: END");
                out.println("END");
                out.flush();
                System.out.println("SERVER: "+in.readLine());
                out.close();
                in.close();
                stdIn.close();
                additionSocket.close();
                System.exit(1);
            }
            //waiting for instruction
            if(state == 0)
            {
                try//find integers from input
                {
                    int z = Integer.parseInt(input);
                    if(z<0)
                    {
                        System.out.println(negativeMessage);
                    }else{
                        state = 1;
                        n1 = z+"";
                        //we have our first number
                    }
                }
                catch(NumberFormatException e)
                {
                    if(input.equals("MR"))
                    {
                        n1 = "MR";
                        state = 1;
                    }else if(input.equals("MC"))//execute the clear command
                    {
                        out.println("MC");
                        
                        System.out.println("SERVER: "+in.readLine());
                        //out.flush();
                    }else if(input.equals("MS"))//execute the store command
                    {
                        System.out.println("Enter value to store (# for value of previous calculation):");
                        String store = "0";
                        while((input=stdIn.readLine())!=null)
                        {
                            try
                            {
                                int z = Integer.parseInt(input);
                                store = z+"";
                                break;
                            }
                            catch(NumberFormatException nFe)
                            {
                                if(input.equals("#"))
                                {
                                    store = "#";
                                    break;
                                }else{
                                    System.out.println("Enter value to store (# for value of previous calculation):");
                                }
                            }
                        }
                        out.println("MS\n"+store);
                        out.flush();
                        System.out.println("SERVER: "+in.readLine());
                        System.out.println(defaultMessage);
                    }else{
                        System.out.println(defaultMessage);
                    }
                }
            }else if(state==1)
            {
                try//has the user input a valid number
                {
                    int z = Integer.parseInt(input);
                    if(z<0)
                    {
                        System.out.println(negativeMessage);
                    }else{
                        n2 = z+"";//we have our second number
                        out.println(n1+"\n+\n"+n2+"\n=");
                        out.flush();
                        System.out.println("SERVER: "+in.readLine());
                        System.out.println(defaultMessage);
                        state = 0;
                    }
                }
                catch(NumberFormatException e)
                {
                    //end equation with MR instead of number
                    if(input.equals("MR"))
                    {
                        n2 = "MR";
                        out.println(n1+"\n+\n"+n2+"\n=");
                        out.flush();
                        System.out.println("SERVER: "+in.readLine());
                        System.out.println(defaultMessage);
                        state = 0;
                    }else if(input.equals("+"))
                    {  
                    }else{           
                        System.out.println(defaultMessage);
                    }
                }
            }
        }
        out.close();
	in.close();
	stdIn.close();
	additionSocket.close();
    }
}
